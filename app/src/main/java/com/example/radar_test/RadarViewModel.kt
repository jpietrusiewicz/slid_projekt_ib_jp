package com.example.radar_test


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.radar_test.RadarItem

class RadarViewModel : ViewModel() {
    var radarItem = MutableLiveData<MutableList<RadarItem>>()
    init {
        radarItem.value = mutableListOf()
    }

    fun addRadarItem(newRadar: RadarItem) {
        val list  = radarItem.value
        list!!.add(newRadar)
        radarItem.postValue(list)
    }

    fun updateRadarItem(newRadar: RadarItem) {
        val list  = radarItem.value
        val radar = list!!.find { it.id == newRadar.id }
        radar!!.time = newRadar.time
        list!!.add(newRadar)
        radarItem.postValue(list)
    }
}