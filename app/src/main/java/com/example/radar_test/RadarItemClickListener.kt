package com.example.radar_test

import com.example.radar_test.RadarItem

interface RadarItemClickListener {
    fun getInfoSatelite( radarItem: RadarItem)
}