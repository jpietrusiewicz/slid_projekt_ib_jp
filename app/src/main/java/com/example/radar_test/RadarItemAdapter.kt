package com.example.radar_test

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.radar_test.databinding.RadarItemCellBinding
import com.example.radar_test.RadarItem
import com.example.radar_test.RadarItemClickListener

class RadarItemAdapter (
    private  val radarItems: List<RadarItem>,
    private val clickListener: RadarItemClickListener
        ):RecyclerView.Adapter<RadarItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RadarItemViewHolder {
        val from  = LayoutInflater.from(parent.context)
        val binding = RadarItemCellBinding.inflate(from, parent, false)
        return RadarItemViewHolder(parent.context, binding, clickListener)
    }

    override fun onBindViewHolder(holder: RadarItemViewHolder, position: Int) {
        holder.bindRadarItem(radarItems[position])
    }

    override fun getItemCount(): Int = radarItems.size
}