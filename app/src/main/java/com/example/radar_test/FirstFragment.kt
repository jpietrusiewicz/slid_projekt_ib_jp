package com.example.radar_test

import android.content.Context
import android.location.*
import android.os.Build
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.radar_test.databinding.FragmentFirstBinding
import com.example.radar_test.RadarItem
import com.example.radar_test.RadarItemClickListener
import com.example.radar_test.RadarViewModel
import java.time.LocalTime
import java.util.concurrent.Executor
import java.util.concurrent.Executors


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@RequiresApi(Build.VERSION_CODES.R)
class FirstFragment : Fragment(), LocationListener, GnssAntennaInfo.Listener, RadarItemClickListener{

    private var _binding: FragmentFirstBinding? = null
    private var _antenasInfo: MutableList<GnssAntennaInfo>? = mutableListOf(GnssAntennaInfo.Builder().build())
    private var _myCallback = MyCallback()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var radarViewModel: RadarViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)


        radarViewModel = ViewModelProvider(this).get(RadarViewModel::class.java)
        setRecyclerView()
        return binding.root

    }

    private fun setRecyclerView() {
        val activity = this
        radarViewModel.radarItem.observe(viewLifecycleOwner) {
            binding.satelistesView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = RadarItemAdapter(it, activity)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.buttonRandom.setOnClickListener {
//            findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
            binding.antenaDesc.setText("")
            binding.antenaDesc.movementMethod = ScrollingMovementMethod()

        }
        /*view.findViewById<Button>(R.id.toast_button).setOnClickListener {
            val myToast = Toast.makeText(context, "Psian dupa", Toast.LENGTH_SHORT)
            myToast.show()
        }*/

        binding.countButton.setOnClickListener {
            countMe(view)
            binding.antenaDesc.movementMethod = ScrollingMovementMethod()
        }
        _myCallback.setContext(context, binding, radarViewModel)

    }

    override fun onGnssAntennaInfoReceived(p0: MutableList<GnssAntennaInfo>) {
        println("Got gnss antenna info")
        _antenasInfo = p0
        val myToast = Toast.makeText(context, "Psia dupa przyszlo onGnssAntenna", Toast.LENGTH_SHORT)
        myToast.show()
        for(antenaInfo: GnssAntennaInfo in p0 ) {
//            binding.textviewFirst.text = antenaInfo.toString()
        }

    }

    override fun onProviderEnabled(provider: String) {
        super.onProviderEnabled(provider)
        val myToast = Toast.makeText(context, "Provider enabled", Toast.LENGTH_SHORT)
        myToast.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun  countMe(view: View) {

//        if (!permissionsGranted()) {
//            ActivityCompat.requestPermissions(this@FirstFragment, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 123)
//        }
//        gnssStatus.getCarrierFrequencyHz(0).toString()
        val executor: Executor =
            Executors.newSingleThreadExecutor()
        executor.execute {
//            val myToast = Toast.makeText(context, "PSIA dupa executor", Toast.LENGTH_SHORT)
//            myToast.show()
            println("psia dupa exec")
        }

        var locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.gnssCapabilities.toString()
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            binding.textviewFirst.text = locationManager.gnssCapabilities.toString()
            var myCallback = MyCallback()
            locationManager.registerGnssMeasurementsCallback(GnssMeasurementRequest.Builder().build(), executor,_myCallback)
        }
        else{
            binding.textviewFirst.text = "GPS disabled"

        }
        val myToast = Toast.makeText(context, locationManager.gnssCapabilities.toString(), Toast.LENGTH_SHORT)
        myToast.show()

//        var meas = GnssMeasurementRequest.Builder().build()
//        textView.text = gnssStatus.getCarrierFrequencyHz(0).toString()

    }




    override fun onLocationChanged(p0: Location) {
        TODO("Not yet implemented")
    }

    private  class MyCallback : GnssMeasurementsEvent.Callback() {
        var _context : Context? = null
        var _binding : FragmentFirstBinding? = null
        private  var radarViewModel: RadarViewModel? = null

        override fun onGnssMeasurementsReceived(eventArgs: GnssMeasurementsEvent?) {
            super.onGnssMeasurementsReceived(eventArgs)
            println("psia dupa przyszlo callback")
            println(eventArgs?.measurements.toString())
            var radarItem = RadarItem(eventArgs?.measurements?.last()?.svid.toString(), eventArgs?.measurements?.last().toString(),eventArgs?.measurements?.last()?.svid.toString(), LocalTime.now())
            radarViewModel?.addRadarItem(radarItem)

            if (_binding?.antenaDesc?.text.toString().isEmpty()) {
                _binding?.antenaDesc?.text = _binding?.antenaDesc?.text.toString() + "\n" + eventArgs?.measurements.toString()
            }
            var countTempStr = _binding?.textviewFirst?.text.toString()
            println("countTempStr $countTempStr")
            var countTemp = countTempStr.toInt()
            countTemp++
            _binding?.textviewFirst?.text = countTemp.toString()
        }

        fun  setContext(context: Context?, binding: FragmentFirstBinding , radarViewModel: RadarViewModel) {
            _binding = binding
            _context = context
            this.radarViewModel = radarViewModel
        }
    }

    override fun getInfoSatelite(radarItem: RadarItem) {
        _binding?.antenaDesc?.text = radarItem.desc
    }
}