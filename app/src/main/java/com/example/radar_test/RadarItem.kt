package com.example.radar_test

import java.time.LocalTime

class RadarItem (
    var name: String,
    var desc: String,
    var id: String,
    var time: LocalTime
)