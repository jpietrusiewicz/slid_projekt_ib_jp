package com.example.radar_test

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.example.radar_test.databinding.RadarItemCellBinding
import com.example.radar_test.RadarItem
import com.example.radar_test.RadarItemClickListener
import java.time.format.DateTimeFormatter

class RadarItemViewHolder(
    private val context: Context,
    private val binding: RadarItemCellBinding,
    private val clickListener: RadarItemClickListener
)
    : RecyclerView.ViewHolder(binding.root){
        val timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss")
        fun bindRadarItem(radarItem: RadarItem){
            binding.radarName.text = radarItem.name

            if(radarItem.time != null)
                binding.radarTime.text = timeFormat.format(radarItem.time)
            else
                binding.radarTime.text = ""

            binding.displayDataButton.setOnClickListener{
                clickListener.getInfoSatelite(radarItem)
            }
        }
}